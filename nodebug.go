// +build !debug

// Disable debug logging
// trick from https://dave.cheney.net/2014/09/28/using-build-to-switch-between-debug-and-release
package main

func dprintf(fmt string, args ...interface{}) {}

func dprint(args ...interface{}) {}

func dprintln(args ...interface{}) {}
