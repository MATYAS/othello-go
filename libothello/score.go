package libothello

import (
	"fmt"
	"math/rand"
	"sort"
)

// Score is the point value of a move.
type Score float64

// HighestScore is the highest possible Score (math.Inf(1) is not a constant)
const HighestScore = Score(1e307)

// LowestScore is the lowest possible Score (math.Inf(-1) is not a constant)
const LowestScore = Score(-1e307)

func (s Score) String() string {
	if s == LowestScore {
		return "-∞"
	} else if s == HighestScore {
		return "+∞"
	} else {
		return fmt.Sprintf("%f", s)
	}
}

// ScoreMin returns the lesser of two Scores.
func ScoreMin(a, b Score) Score {
	if a > b {
		return b
	}
	return a
}

// ScoreMax returns the greater of two Scores
func ScoreMax(a, b Score) Score {
	if a < b {
		return b
	}
	return a
}

// ScoredMove is a move with an associated point value.
type ScoredMove struct {
	Move
	Score
}

func (sm ScoredMove) String() string {
	return fmt.Sprintf("%v: %v", sm.Move, sm.Score)
}

// ScoredMoveList is a list of scored moves.
type ScoredMoveList []ScoredMove

// MoveToScoreMap is a map of Moves to their associated Scores.
type MoveToScoreMap map[Move]Score

// GetSortedList returns a list of ScoredMoves, lowest first.
func (m MoveToScoreMap) GetSortedList() ScoredMoveList {
	l := make(ScoredMoveList, 0, len(m))
	for move, score := range m {
		l = append(l, ScoredMove{move, score})
	}
	sort.Slice(l, func(i, j int) bool { return l[i].Score < l[j].Score })
	return l
}

// GetReverseSortedList returns a list of ScoredMoves, highest first.
func (m MoveToScoreMap) GetReverseSortedList() ScoredMoveList {
	l := make(ScoredMoveList, 0, len(m))
	for move, score := range m {
		l = append(l, ScoredMove{move, score})
	}
	sort.Slice(l, func(i, j int) bool { return l[i].Score > l[j].Score })
	return l
}

func (m MoveToScoreMap) String() string {
	return fmt.Sprint(m.GetSortedList())
}

// PickBestMove returns the Move and Score of the move with the highest
// score (picking one at random in case of a tie).
func PickBestMove(smlist []ScoredMove) (move Move, score Score) {
	if len(smlist) == 1 {
		// dprintln("one move")
		return smlist[0].Move, smlist[0].Score
	}

	bestScore := LowestScore
	var bestMoves []Move

	for _, sm := range smlist {
		// msstr := fmt.Sprintf("move %v score %v", sm.move, sm.score)
		if sm.Score < bestScore {
			// dprintf("%s not good enough (< %v)\n", msstr, bestScore)
			continue
		} else if sm.Score == bestScore {
			// dprintln(msstr, "added")
			bestMoves = append(bestMoves, sm.Move)
		} else { // sm.score > bestScore
			// dprintln(msstr, "new best")
			bestScore = sm.Score
			bestMoves = []Move{sm.Move}
		}
	}
	return bestMoves[rand.Intn(len(bestMoves))], bestScore
}

// GreedyScore gives the score of the pieces on the board based on number
// of pieces on each side. Positive is better for current player.
func GreedyScore(brd Board) Score {
	return Score(int(Bitcount(brd.Current)) - int(Bitcount(brd.Opponent)))
}

// WeightedScore gives the score of the pieces on the board based
// on number of pieces on each side, with extras weight given to edge
// pieces and corner pieces. Positive is better for current player.
func WeightedScore(brd Board) Score {
	const cornerMask Bitmap = 0x8100000000000081
	const edgeMask Bitmap = 0xff818181818181ff

	weighted := func(bmp Bitmap) int {
		return int(Bitcount(bmp) + 4*Bitcount(edgeMask&bmp) + 15*Bitcount(cornerMask&bmp))
	}
	return Score(weighted(brd.Current) - weighted(brd.Opponent))
}
