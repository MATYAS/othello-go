// +build debug

// debug logging
// trick from https://dave.cheney.net/2014/09/28/using-build-to-switch-between-debug-and-release
// build with "-tags debug" to activate

package libothello

import (
	"fmt"
	"log"
	"os"
)

func dprintf(f string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, f, args...)
}

func dprint(args ...interface{}) {
	fmt.Fprint(os.Stderr, args...)
}

func dprintln(args ...interface{}) {
	fmt.Fprintln(os.Stderr, args...)
}

func panicOnIllegalMove(g *Game, m Move) {
	if !g.IsLegalMove(m) {
		whoseTurn := g.WhoseTurn
		var brdB, brdW Bitmap
		if whoseTurn == Black {
			brdB, brdW = g.Board.Current, g.Board.Opponent
		} else {
			brdB, brdW = g.Board.Opponent, g.Board.Current
		}
		log.Panicf(
			`attempt to play illegal move
board:
--- black
%v
--- white
%v

move:
%v %v
`, brdB, brdW, whoseTurn, m)
	}
}
