package libothello

import (
	"fmt"
)

// Side is one color of game pieces.
type Side int

// Black and White are the two colors for game pieces.
const (
	Black Side = 0
	White      = 1
)

// Other is the side of the other player.
func (s Side) Other() Side {
	return 1 - s
}

func (s Side) String() string {
	if s == Black {
		return "black"
	}
	return "white"
}

// Board represents the state of an Othello board as a pair of Bitmaps,
// one representing all the pieces of the current player and one for all
// the pieces of the opponent.
type Board struct {
	Current  Bitmap
	Opponent Bitmap
}

// IsValid checks that a Board contains no overlapping pieces.
func (b Board) IsValid() bool {
	return b.Current&b.Opponent == 0
}

// EmptyMap returns the unoccupied spaces.
func (b Board) EmptyMap() Bitmap {
	return ^(b.Current | b.Opponent)
}

// PieceCount counts the number of pieces in a board.
// Game.PieceCount is faster so use if you can.
func (b Board) PieceCount() int {
	return 64 - Bitcount(b.EmptyMap())
}

// Game represents the state of the Othello game.
type Game struct {
	// Board is the bitboard representing the locations of the pieces of
	// both players.
	Board
	// GameOver is true if no further legal moves can be made.
	GameOver bool
	// WhoseTurn is the Side of the current player.
	WhoseTurn Side
	// TurnCount starts at 1 for a new game and is incremented after every
	// move, even if that move is a pass.
	TurnCount uint
	// PieceCount is the total number of pieces on the board. Some
	// algorithms make use of this.
	PieceCount int
	// LastMove the most recent move (by the opposing player).  If it was a
	// pass, another pass will end the game.
	LastMove Move
}

// NewGame sets up the initial positions.
func NewGame() Game {
	var black, white Bitmap

	white = 0x0000001008000000 // pieces at D4 and E5
	black = 0x0000000810000000 // pieces at E4 and D5

	return Game{Board: Board{Current: black, Opponent: white}, PieceCount: 4, TurnCount: 1}
}

// Move represents a move by a player. Should be a single bit or empty for a pass.
type Move uint64

// PassingMove represents a move where a player was forced to pass.
const PassingMove Move = 0x0

// IsValid returns if a move is a single bit or a pass.
func (m Move) IsValid() bool {
	return Bitcount(Bitmap(m)) <= 1
}

// IsLegalMove returns if a move is a permissible move for the game.
func (g *Game) IsLegalMove(m Move) bool {
	legals := uint64(g.Legals())
	mm := uint64(m)
	return m.IsValid() && ((legals == mm) || (legals&mm != 0))
}

func (m Move) String() string {
	pos, empty := Bitmap(m).ToPos()
	if empty {
		return "PASS"
	}
	return fmt.Sprintf("%c%d", pos.X+65, pos.Y+1)
}

// ApplyMove applies a move to the game state.
//
// If the game is already over, nothing happens.  Otherwise, g.TurnCount is
// incremented, play switches to the opponent (flipping g.WhoseTurn, and
// swapping g.Board.Current with g.Board.Opponent), and g.LastMove is set to m.
// If m is a pass, no pieces are placed. Also, if g.LastMove was a pass, the
// game ends (because neither player could make a valid move).  If m is not a
// pass, a piece is placed and surrounding pieces flip according to the rules.
// g.PieceCount is incremented.
func (g *Game) ApplyMove(m Move) {
	if g.GameOver {
		return
	}

	// This has no effect in non-debug mode.
	panicOnIllegalMove(g, m)

	lastpass := g.LastMove == PassingMove
	g.LastMove = m
	g.TurnCount++

	if m != PassingMove {
		bit := Bitmap(m)
		f := g.Flips(m)
		g.Board.Current |= f | bit
		g.Board.Opponent &^= f
		g.PieceCount++
	} else if lastpass {
		// Neither player could make a move.
		g.GameOver = true
	}

	if g.Board.EmptyMap() == 0 {
		// Board filled.
		g.GameOver = true
	} else if g.Board.Opponent == Bitmap(0x0) {
		// Opponent has no pieces left.
		g.GameOver = true
	}
	g.WhoseTurn = g.WhoseTurn.Other()
	g.Board.Current, g.Board.Opponent = g.Board.Opponent, g.Board.Current
}

// PieceCounts returns the number of pieces each player has on the board.
// Black first, then white.
func (g *Game) PieceCounts() (black int, white int) {
	var blackPieces, whitePieces Bitmap
	if g.WhoseTurn == Black {
		blackPieces, whitePieces = g.Board.Current, g.Board.Opponent
	} else {
		blackPieces, whitePieces = g.Board.Opponent, g.Board.Current
	}
	return Bitcount(blackPieces), Bitcount(whitePieces)
}

// Code for getting legal moves and flipping pieces.  Generated because loops
// are unrolled to increase performance.
//go:generate ../utils/pgo2go legalsflips.pgo

// vim:ft=go:sts=8:sw=8:noet
