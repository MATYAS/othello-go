package libothello

import (
	"fmt"
	"math/bits"
)

// Bitmap is an 8x8 array of bits used to represent parts of the game
// board.  The most significant bit represents (x=0, y=0), the upper left
// of the board.
type Bitmap uint64

const highestBit Bitmap = 1 << 63

// SplitNaive splits a Bitmap into Bitmaps of individual bits.
// Naïve algorithm
func (bmp Bitmap) SplitNaive() []Bitmap {
	if bmp == 0 {
		return []Bitmap{}
	}
	var result []Bitmap
	for bit := highestBit; bit > 0; bit >>= 1 {
		if bmp&bit != 0 {
			result = append(result, bit)
		}
	}
	return result
}

// Split splits a Bitmap into Bitmaps of individual bits.
// K&R algorithm via tannenba. Thanks Todd!
func (bmp Bitmap) Split() []Bitmap {
	var result []Bitmap
	for bmp != 0 {
		bit := bmp &^ (bmp - 1)
		result = append(result, bit)
		bmp &= bmp - 1
	}
	return result
}

// MSplit splits a Bitmap into Moves of individual bits.
// K&R algorithm via tannenba. Thanks Todd!
// Special case: return the passing move if there are no
// moves. Also return number of moves.
func (bmp Bitmap) MSplit() ([]Move, int) {
	var result []Move
	var nmoves int
	if bmp == 0 {
		return []Move{PassingMove}, 1
	}
	// 83% of observed board states had 12 or fewer legal moves.
	result = make([]Move, 0, 12)
	for bmp != 0 {
		bit := bmp &^ (bmp - 1)
		result = append(result, Move(bit))
		bmp &= bmp - 1
		nmoves++
	}
	return result, nmoves
}

func (bmp Bitmap) String() string {
	var result string
	var mask uint64
	var y uint

	mask = 0xff << 56
	b := uint64(bmp)

	for y = 7; ; y-- {
		var shf = y << 3
		var trunc uint8
		trunc = (uint8)((b & mask) >> shf)
		result += fmt.Sprintf("%08b\n", trunc)
		mask >>= 8
		if y == 0 {
			break
		}
	}

	return result
}

// Bitcount counts the number of 1 bits in a Bitmap (64-bit int).
// Return a signed int for convenience (since this is often used as score).
func Bitcount(bmp Bitmap) int {
	return int(bits.OnesCount64(uint64(bmp)))
}

// Pos represents an X, Y coordinate.
type Pos struct {
	X, Y uint
}

// ToBitmap converts a Pos into a Bitmap of a single bit.
func (p Pos) ToBitmap() Bitmap {
	return (1 << (7 - p.X)) << ((7 - p.Y) << 3)
}

/*
// ToPosList converts a Bitmap into a list of positions.
func (bmp Bitmap) ToPosList() []Pos {
	if bmp == 0 {
		return []Pos{}
	}
	var result []Pos

	var x, y, i uint
	for i = 0; i < 64; i++ {
		x, y = i%8, i/8
		if bmp&highestBit != 0 {
			result = append(result, Pos{x, y})
		}
		bmp <<= 1
	}

	return result
}
*/

// ToPos converts a Bitmap of a single bit into a Pos.
// empty is true if the Bitmap contains no set bits.
// Undefined behavior if Bitmap contains multiple bits.
func (bmp Bitmap) ToPos() (p Pos, empty bool) {
	if bmp == 0 {
		return Pos{0, 0}, true
	}
	var x, y, i uint
	for i = 0; i < 64; i++ {
		x, y = i%8, i/8
		if bmp&highestBit != 0 {
			return Pos{x, y}, false
		}
		bmp <<= 1
	}
	return Pos{0, 0}, true // shouldn't get here
}

// vim:ft=go:sts=8:sw=8:noet
