#!/usr/bin/perl

# Transform raw benchmark results into a table (tab-separated) with
# headers suitable for parsing in R with something like
#   benchmarks <- read.table("minimax.tab", sep="\t", header=TRUE)

# go test needs to be run with -benchmem.
# The benchmark needs to be specified to this script with -b so we
# know which headers to use and parameters to extract.

# Usage: ./benchmarks.pl -b MinimaxMover results/benchmarks.raw
# (writes results/benchmarks.tab)

use warnings;
use warnings FATAL => qw(uninitialized);
use English qw(-no_match_vars);
use strict;
use Getopt::Long;
use 5.010;  # RHEL 6
#use diagnostics;

$\ = $/;

our $bm;
our $out;

GetOptions("benchmark|b=s" => \$bm,
           "out|o=s" => \$out,
           );

our %bm_params = ( minimaxmover => { headers => [qw(Depth Turns)]
                                   , replace => sub { s/d=(\d+)_tu=(\d+)-\d+/$1 $2/ }
                                   }
                 , profileminimax => { headers => ["Turns"]
                                     , replace => sub { s/d=\d+_tu=(\d+)-\d+/$1/ }
                                     }
                 , profilemcts => { headers => ["Turns"]
                                  , replace => sub { s/par_t=\d+_tu=(\d+)-\d+/$1/ }
                                  }
                 , generic => { headers => ["Benchmark"]
                              , replace => sub {s/(\S+)-\d+/$1/ }
                              }
                 );

$bm ||= "generic";
$bm = lc $bm;
die "unknown benchmark $bm" unless exists($bm_params{$bm});

our @common_headers = qw(Iters Time Mem Allocs);

$#ARGV == 0 or die "invalid number of arguments; need 1 file";
my $in = $ARGV[0];
unless (defined($out)) {
    $out = $in;
    $out =~ s/\.raw$//;
    $out .= ".tab";
}
open(IN, '<', $in) or die "Couldn't open input file: $!";
open(OUT, '>', $out) or die "Couldn't open output file: $!";

print OUT join("\t", @{$bm_params{$bm}{headers}}, @common_headers);
for (<IN>) {
    $_ = lc $_;
    
    next unless /^benchmark/;
    next if /fuzz/;

    s/^benchmark//;
    s/ns\/op//;
    s/b\/op//;
    s/allocs\/op//;
    if (/^$bm\// || $bm eq "generic") {
        s/$bm\///;
        &{$bm_params{$bm}{replace}};
        my @elems = split ' ';
        print OUT join("\t", @elems);
    }
}
print "Wrote $out";

