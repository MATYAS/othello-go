package main

import (
	"fmt"
	"strings"

	lo "othello/libothello"
)

/****************************************************************************
*
* OUTPUT
*
****************************************************************************/

// Line drawing characters
const (
	UL     = "┌"
	HORIZ  = "─"
	HDOWN  = "┬"
	UR     = "┐"
	VERT   = "│"
	VRIGHT = "├"
	CROSS  = "┼"
	VLEFT  = "┤"
	LL     = "└"
	HUP    = "┴"
	LR     = "┘"
)

// Strings for pieces.
// They are two characters long because some of the symbols are wide
// characters.
const (
	OPENCIR   = "◯ "
	FILLEDCIR = "⬤ "
	DOTTEDCIR = "◌ "
	SMALLSQR  = "▫ "
)

// ANSI codes
const (
	RESETCOLOR = "\x1B[0m"
	PURPLEBG   = "\x1B[45m"
	GREENBG    = "\x1B[42m"
	WHITEFG    = "\x1B[1;97m"
	BLACKFG    = "\x1B[30m"
	YELLOWFG   = "\x1B[1;93m"
	GREYFG     = "\x1B[37m"
)

// ┌─┬─┐
// │ │ │
// ├─┼─┤
// │ │ │
// └─┴─┘

type prettyBoardSquare byte

const (
	emptySquare prettyBoardSquare = iota
	currentSquare
	opponentSquare
	legalSquare
)

type prettyBoard [][]prettyBoardSquare

// newPrettyBoard converts a bitboard "brd" and a bitmap of legal moves
// "legals" into a prettyBoard (an 8x8 array of an enum).
func newPrettyBoard(brd lo.Board, legals lo.Bitmap) prettyBoard {
	pb := make(prettyBoard, 8)
	for x := 0; x < 8; x++ {
		pb[x] = make([]prettyBoardSquare, 8)
	}

	var i lo.Bitmap
	var idx int
	for i, idx = 1<<63, 0; i > 0; i, idx = i>>1, idx+1 {
		x, y := idx%8, idx/8
		if (brd.Current & i) != 0 {
			pb[x][y] = currentSquare
		} else if (brd.Opponent & i) != 0 {
			pb[x][y] = opponentSquare
		} else if (legals & i) != 0 {
			pb[x][y] = legalSquare
		} else {
			pb[x][y] = emptySquare
		}
	}

	return pb
}

// printLetters prints the X axis labels (letters A through H).  If "rotate" is
// true, then the board has been rotated so the letters are printed in reverse.
func printLetters(rotate bool) {
	fmt.Print("  ")
	var x int
	for x = 0; x < 8; x++ {
		ch := x + 65
		if rotate {
			ch = 64 + 8 - x
		}
		fmt.Printf("  %c", ch)
	}
	fmt.Println()
}

// printCoords prints the numeric X axis labels (0 through 7).  If "rotate" is
// true, then the board has been rotated so the numbers are printed in reverse.
// This is used if we're using the "coords" display mode.
func printCoords(rotate bool) {
	fmt.Print("  ")
	for x := 0; x < 8; x++ {
		if rotate {
			fmt.Printf(" %2d", 7-x)
		} else {
			fmt.Printf(" %2d", x)
		}
	}
	fmt.Println()
}

// ConsoleOptions are parameters for interactive console I/O
type ConsoleOptions struct {
	bow         bool
	coords      bool
	nocolor     bool
	nopause     bool
	notable     bool
	resultsOnly bool
	rotate      bool
}

// symbols is a table of which symbols to use for pieces owned by the current
// player, pieces owned by the opponent, and valid moves.  This is based on the
// ConsoleOptions and whether the current player is white or black.
var symbols = []struct {
	bow       bool
	nocolor   bool
	whoseTurn lo.Side
	current   string
	opponent  string
	legal     string
}{
	{false, false, lo.Black, BLACKFG + FILLEDCIR, WHITEFG + FILLEDCIR, BLACKFG + OPENCIR},
	{false, false, lo.White, WHITEFG + FILLEDCIR, BLACKFG + FILLEDCIR, WHITEFG + OPENCIR},
	{false, true, lo.Black, OPENCIR, FILLEDCIR, SMALLSQR},
	{false, true, lo.White, FILLEDCIR, OPENCIR, SMALLSQR},
	{true, false, lo.Black, WHITEFG + FILLEDCIR, BLACKFG + FILLEDCIR, WHITEFG + OPENCIR},
	{true, false, lo.White, BLACKFG + FILLEDCIR, WHITEFG + FILLEDCIR, BLACKFG + OPENCIR},
	{true, true, lo.Black, FILLEDCIR, OPENCIR, SMALLSQR},
	{true, true, lo.White, OPENCIR, FILLEDCIR, SMALLSQR},
}

// PrintBoard prints the current board onto the console.
// The last move, if specified, is highlighted.  Since "b" contains "current"
// and "opponent" pieces, we need "whoseTurn" to find outh whether "current" is
// white or black.
func (con *ConsoleOptions) PrintBoard(b *prettyBoard, lastMove lo.Bitmap, whoseTurn lo.Side) {
	// Print the X axis labels on the top.
	if con.coords {
		printCoords(con.rotate)
	} else {
		printLetters(con.rotate)
	}

	// Determine the strings to output (color code + symbol) for current
	// player piece, opponent piece, and legal move, using the "symbols"
	// table.
	var cursym, oppsym, legsym string
	for _, s := range symbols {
		if con.bow == s.bow && con.nocolor == s.nocolor && whoseTurn == s.whoseTurn {
			cursym, oppsym, legsym = s.current, s.opponent, s.legal
			break
		}
	}
	boardcolor, labelcolor, endl := GREYFG+GREENBG, RESETCOLOR, RESETCOLOR+"\n"
	if con.nocolor {
		boardcolor, labelcolor, endl = "", "", "\n"
	}

	var x, y int
	// Print the top border of the board.
	fmt.Print("  " + boardcolor + UL + HORIZ)
	for x = 1; x < 8; x++ {
		fmt.Print(HORIZ + HDOWN + HORIZ)
	}
	fmt.Print(HORIZ + UR + endl)

	lastMovePos, lastMoveEmpty := lastMove.ToPos()
	var xx int
	var yy int
	for y = 0; y < 8; y++ {
		// yy is the y index of the piece we're looking at.
		if con.rotate {
			yy = 7 - y
		} else {
			yy = y
		}

		// Print the Y axis label for this row on the left, and the
		// left border of the board.
		if con.coords {
			fmt.Printf("%s%-2d%s", labelcolor, yy, boardcolor+VERT)
		} else {
			fmt.Printf("%s%-2d%s", labelcolor, yy+1, boardcolor+VERT)
		}

		for x = 0; x < 8; x++ {
			// xx is the x index of the piece we're looking at.
			if con.rotate {
				xx = 7 - x
			} else {
				xx = x
			}

			// Highlight the most recent move in a different color
			// (if we're using colors).
			if !con.nocolor {
				if !lastMoveEmpty && lastMovePos.X == uint(xx) && lastMovePos.Y == uint(yy) {
					fmt.Print(PURPLEBG)
				} else {
					fmt.Print(GREENBG)
				}
			}
			// Draw the current piece.
			switch (*b)[xx][yy] {
			case emptySquare:
				fmt.Print("  ")
			case currentSquare:
				fmt.Print(cursym)
			case opponentSquare:
				fmt.Print(oppsym)
			case legalSquare:
				fmt.Print(legsym)
			}
			// Draw the vertical separator between columns.
			fmt.Print(boardcolor + VERT)
		}

		// Print the Y axis label for this row on the right.
		if con.coords {
			fmt.Printf("%s%2d%s", labelcolor, yy, endl)
		} else {
			fmt.Printf("%s%2d%s", labelcolor, yy+1, endl)
		}

		// Draw the horizontal separator between rows and the cross
		// separator if in the middle of the board.  (The bottom gets
		// printed outside the loop).
		if y != 7 {
			fmt.Print("  " + boardcolor + VRIGHT + HORIZ)
			for x = 1; x < 8; x++ {
				fmt.Print(boardcolor + HORIZ + CROSS + HORIZ)
			}
			fmt.Print(boardcolor + HORIZ + VLEFT + endl)
		}
	}

	// Print the bottom border of the board.
	fmt.Print("  " + boardcolor + LL + HORIZ)
	for x = 1; x < 8; x++ {
		fmt.Print(HORIZ + HUP + HORIZ)
	}
	fmt.Print(HORIZ + LR + endl)

	// Print the X axis labels on the bottom.
	fmt.Print(labelcolor)
	if con.coords {
		printCoords(con.rotate)
	} else {
		printLetters(con.rotate)
	}
}

// PrintGame pretty-prints the game to console.
// This includes the last move (and who made it), the turn number, how many
// pieces each player has, and whose next move it is.
func (con *ConsoleOptions) PrintGame(gPtr *lo.Game) {
	blackPieces, whitePieces := gPtr.PieceCounts()
	lastMove := lo.Bitmap(gPtr.LastMove)
	fmt.Printf("           TURN %2d\n", gPtr.TurnCount)
	fmt.Printf(" BLACK %2d          WHITE %2d\n", blackPieces, whitePieces)
	b := newPrettyBoard(gPtr.Board, gPtr.Legals())
	con.PrintBoard(&b, lastMove, gPtr.WhoseTurn)
	if gPtr.GameOver {
		fmt.Println("\tGAME OVER")
	} else {
		fmt.Printf("\t%s'S MOVE\n", strings.ToUpper(gPtr.WhoseTurn.String()))
	}
}

// PrintMove pretty-prints a move to the console.
func (con *ConsoleOptions) PrintMove(m lo.Move, s lo.Side) {
	fmt.Printf("%s ", strings.ToUpper(s.String()))
	pos, empty := lo.Bitmap(m).ToPos()
	if empty {
		fmt.Println("PASSES")
	} else if con.coords {
		fmt.Printf("MOVES R=%d,C=%d", pos.Y, pos.X)
	} else {
		fmt.Printf("MOVES %c%d", pos.X+65, pos.Y+1)
	}
}

// vim:ft=go:sts=8:sw=8:noet
