package mover

import (
	lo "othello/libothello"
	"time"
)

// Mover is an interface that contains PickMove for determining a move
// based on game state.
type Mover interface {
	// Init handles command-line options and/or creates data structures.
	Init(opts map[string]string)
	// PickMove picks the best move in response to the game state g within
	// the time remaining.
	PickMove(g *lo.Game, timeRemaining time.Duration) lo.Move
	// Copy is a deep copy of the data structures of the Mover.
	Copy() Mover
}

// vim:ft=go:sts=8:sw=8:noet
