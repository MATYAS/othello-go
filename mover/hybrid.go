package mover

import (
	lo "othello/libothello"
	"strconv"
	"time"
)

// HybridMover is a combination of the IDDFSMover and the UCTMover, using the
// former in the beginning of the game and the latter in the rest.
type HybridMover struct {
	// ChangeAtPC is at how many pieces we should switch from IDDFS to UCT.
	ChangeAtPC int
	*IDDFSMover
	*UCTMover
	currentMover Mover
}

// Init sets the options for HybridMover.
func (m *HybridMover) Init(opts map[string]string) {
	m.ChangeAtPC = 20
	m.IDDFSMover = new(IDDFSMover)
	m.IDDFSMover.Init(opts)
	m.UCTMover = new(UCTMover)
	m.UCTMover.Init(opts)
	m.currentMover = m.IDDFSMover

	val, ok := opts["change"]
	if ok {
		var err error
		m.ChangeAtPC, err = strconv.Atoi(val)
		if err != nil {
			panic("error: " + err.Error())
		}
		dprintln("hybrid: set change to", m.ChangeAtPC)
	}
}

// Copy creates a copy with its own data structures.
func (m *HybridMover) Copy() Mover {
	newm := *m
	var ok bool
	newm.IDDFSMover, ok = (m.IDDFSMover.Copy()).(*IDDFSMover)
	if !ok {
		panic("m.IDDFSMover wasn't??")
	}
	newm.UCTMover, ok = (m.UCTMover.Copy()).(*UCTMover)
	if !ok {
		panic("m.UCTMover wasn't??")
	}
	return &newm
}

// PickMove for HybridMover.
func (m *HybridMover) PickMove(gPtr *lo.Game, timeRemaining time.Duration) lo.Move {

	if m.currentMover != m.UCTMover && gPtr.PieceCount >= m.ChangeAtPC {
		dprintln("hybrid: changing")
		m.currentMover = m.UCTMover
		// also clear the data structures of the IDDFSMover to save memory.	}
		m.IDDFSMover.Clear()
	}

	return m.currentMover.PickMove(gPtr, timeRemaining)
}
