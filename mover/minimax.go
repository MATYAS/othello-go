package mover

import (
	"math/rand"
	lo "othello/libothello"
	"strconv"
	"time"
)

/***************************************************************************
 * Minimax mover w/ alpha-beta pruning
 ***************************************************************************/

// MinimaxMover uses the minimax algorithm to look ahead a certain number
// of moves. A weighted evaluation function is used, except for the
// endgame, where a greedy score is used.
type MinimaxMover struct {
	Depth uint
}

// Init sets the max depth for MinimaxMover.
func (m *MinimaxMover) Init(opts map[string]string) {
	m.Depth = 7
	val, ok := opts["d"]
	if ok {
		tmp, err := strconv.ParseUint(val, 10, 64)
		if err != nil {
			panic("error: " + err.Error())
		}
		m.Depth = uint(tmp)
		dprintln("minimax: set depth to", m.Depth)
	}
}

// Copy creates a copy of this mover.
func (m *MinimaxMover) Copy() Mover {
	newm := *m
	return &newm
}

// alpha-beta pruning from
//   Stuart Russel and Peter Norvig. "Alpha-Beta Pruning" in _Artificial Intelligence: A Modern Approach_ 2nd ed.
//     Upper Saddle River, NJ: Pearson Education, 2003, pp. 167-171.
func (m *MinimaxMover) minimaxAB(depth, maxdepth uint, gPtr *lo.Game, min bool, alpha, beta lo.Score) lo.Score {
	if gPtr.GameOver {
		return 1024.0 * lo.GreedyScore(gPtr.Board)
	}
	if depth >= maxdepth {
		return lo.WeightedScore(gPtr.Board)
	}

	bestScore := lo.LowestScore
	if min {
		bestScore = lo.HighestScore
	}

	legalsList, legalsLen := gPtr.LegalMoves()

	if legalsLen == 1 {
		maxdepth++
	}

	for _, move := range legalsList {
		// dprintAlphaBeta(depth, move, min, alpha, beta)
		gNext := *gPtr
		gNext.ApplyMove(move)
		score := m.minimaxAB(depth+1, maxdepth, &gNext, !min, alpha, beta)
		// dprintfIndent(depth+1, "%-3s %4d\n", move, score)

		// These if statements do not actually drop performance! I tried
		// separating minimaxAB into minAB and maxAB but saw no performance
		// gain! See split-minimax git tag for proof.
		if (min && score < bestScore) || (!min && score > bestScore) {
			bestScore = score
		}
		if min {
			if bestScore <= alpha {
				// dprintfIndent(depth+1, "    ⬇︎ pruning (⍺ >= %4d)\n", bestScore)
				return bestScore
			}
			beta = lo.ScoreMin(beta, bestScore)
		} else {
			if bestScore >= beta {
				// dprintfIndent(depth+1, "    ⬆︎ pruning (β <= %4d)\n", bestScore)
				return bestScore
			}
			alpha = lo.ScoreMax(alpha, bestScore)
		}
	}
	return bestScore
}

// PickMove for MinimaxPlayer.
func (m *MinimaxMover) PickMove(gPtr *lo.Game, timeRemaining time.Duration) lo.Move {
	legalsList, legalsLen := gPtr.LegalMoves()

	if legalsLen == 1 {
		return legalsList[0]
	}

	alpha := lo.LowestScore
	bestScore := lo.LowestScore
	var bestMove lo.Move

	// randomize order of possible moves
	for _, moveIdx := range rand.Perm(legalsLen) {
		move := legalsList[moveIdx]
		gNext := *gPtr
		gNext.ApplyMove(move)
		// dprintf("\n%-3s⬆︎\n", move)
		score := m.minimaxAB(0, m.Depth, &gNext, true, alpha, lo.HighestScore)
		if score > bestScore {
			bestMove, bestScore = move, score
		}
		// dprintf("%-3s⬆︎%4d\n", move, score)

		// This is a "max" node, so beta never gets updated.
		alpha = lo.ScoreMax(alpha, bestScore)
	}

	return bestMove
}
