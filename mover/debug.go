// +build debug

// debug logging
// trick from https://dave.cheney.net/2014/09/28/using-build-to-switch-between-debug-and-release
// build with "-tags debug" to activate

package mover

import (
	"fmt"
	"os"
	lo "othello/libothello"
)

func dprintf(f string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, f, args...)
}

func dprint(args ...interface{}) {
	fmt.Fprint(os.Stderr, args...)
}

func dprintln(args ...interface{}) {
	fmt.Fprintln(os.Stderr, args...)
}

func indentStr(n uint) string {
	s := ""
	for i := uint(0); i < n; i++ {
		s += "  "
	}
	return s
}

func dprintfIndent(indent uint, f string, args ...interface{}) {
	fmt.Fprint(os.Stderr, indentStr(indent))
	fmt.Fprintf(os.Stderr, f, args...)
}

func dprintAlphaBeta(depth uint, move lo.Move, min bool, alpha, beta lo.Score) {
	dirstr := "⬆︎"
	if min {
		dirstr = "⬇︎"
	}

	dprintfIndent(depth+1, "%-3s%s      ⍺ = %4v  β = %4v\n", move, dirstr, alpha, beta)
}
