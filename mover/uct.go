package mover

import (
	"log"
	"math"
	"math/rand"
	lo "othello/libothello"
	"runtime"
	"strconv"
	"sync"
	"time"
)

// VisWin is a pair of how many times a node has been visited and how many
// times a game has been won from it.
type VisWin struct {
	visits int
	wins   int
}

// WinRate is wins/visits.
func (v VisWin) WinRate() float64 {
	return float64(v.wins) / float64(v.visits)
}

// UCTStateTable tracks visits and wins for each game state visited
// as part of the evaluation. Contains a mutex because it may be updated by
// multiple threads.
type UCTStateTable struct {
	table map[lo.Board]VisWin
	mutex sync.Mutex
}

// UCTStateTableArray is an array of state tables, one element for each piece
// count. Tables can be deleted after each move because we will never return
// to that same piece count.
type UCTStateTableArray [65]*UCTStateTable

// NewUCTStateTableArray creates a new UCTStateTableArray, initializing all
// elements to an empty map.
func NewUCTStateTableArray() UCTStateTableArray {
	sta := UCTStateTableArray{}
	for i := range sta {
		sta[i] = new(UCTStateTable)
		sta[i].table = make(map[lo.Board]VisWin, 0)
	}
	return sta
}

// Clear deletes an unnecessary table.
func (sta *UCTStateTableArray) Clear(pieceCount int) {
	sta[pieceCount] = new(UCTStateTable)
}

// SetVisWin sets the VisWin for a board in a table.
func (sta *UCTStateTableArray) SetVisWin(brd lo.Board, vw VisWin) {
	st := sta[brd.PieceCount()]
	st.mutex.Lock()
	st.table[brd] = vw
	st.mutex.Unlock()
}

// BumpVisWin increases the VisWin for a board in a table.
func (sta *UCTStateTableArray) BumpVisWin(brd lo.Board, wonOrLost int) {
	st := sta[brd.PieceCount()]
	st.mutex.Lock()
	vw, ok := st.table[brd]
	if ok {
		vw.visits++
		vw.wins += wonOrLost
		st.table[brd] = vw
	}
	st.mutex.Unlock()
}

// VisWin gets the VisWin for a board in a table.
func (sta *UCTStateTableArray) VisWin(brd lo.Board) (vw VisWin, ok bool) {
	st := sta[brd.PieceCount()]
	st.mutex.Lock()
	vw, ok = st.table[brd]
	st.mutex.Unlock()
	if !ok {
		return VisWin{0, 0}, false
	}
	return vw, true
}

// UCTMover implements Upper Confidence bounds for Trees
// TODO Citation needed
type UCTMover struct {
	Runs            int
	Parallel        bool
	ExploreConstant float64
	IncreaseRuns    bool
	outOfTime       bool
	timeoutTimer    *time.Timer
	sta             UCTStateTableArray
	side            lo.Side
}

// Init sets the number of trees evaluated for UCTMover.
func (m *UCTMover) Init(opts map[string]string) {
	m.Runs = 15000
	m.Parallel = runtime.GOMAXPROCS(0) > 1
	m.sta = NewUCTStateTableArray()
	m.ExploreConstant = 6.0

	var val string
	var ok bool
	var err error
	val, ok = opts["r"]
	if ok {
		m.Runs, err = strconv.Atoi(val)
		if err != nil {
			panic("error: " + err.Error())
		}
		dprintln("uct: set runs to", m.Runs)
	}
	val, ok = opts["C"]
	if ok {
		m.ExploreConstant, err = strconv.ParseFloat(val, 64)
		if err != nil {
			panic("error: " + err.Error())
		}
		dprintln("uct: set exploreConstant to", m.ExploreConstant)
	}
	val, ok = opts["ir"]
	if ok {
		tmp, err := strconv.Atoi(val)
		if err != nil {
			panic("error: " + err.Error())
		}
		if tmp > 0 {
			m.IncreaseRuns = true
		} else {
			m.IncreaseRuns = false
		}
		dprintln("uct: set increaseRuns to", m.IncreaseRuns)
	}
}

// Copy creates a copy of this mover with its own STA.
func (m *UCTMover) Copy() Mover {
	newm := *m
	newm.sta = NewUCTStateTableArray()

	// m.sta is an array of pointers to UCTStateTables (a struct)
	for i, st := range m.sta {
		// st (a *UCTStateTable) contains a .table and a .mutex
		// mutexes shouldn't be copied. Table is a map of Board (struct)
		// to VisWin (another struct). Simple assignment will copy each
		// element.
		for k, v := range st.table {
			newm.sta[i].table[k] = v
		}
	}
	return &newm
}

// https://jeffbradberry.com/posts/2015/09/intro-to-monte-carlo-tree-search/
func (m *UCTMover) simulate(gPtr *lo.Game) {
	visited := make([]lo.Board, 0, 16)

	expand, wonOrLost := true, 0
	for {
		_, ok := m.sta.VisWin(gPtr.Board)
		if ok {
			visited = append(visited, gPtr.Board)
		} else if !ok && expand { // only expand once per sim run
			m.sta.SetVisWin(gPtr.Board, VisWin{0, 0})
			visited = append(visited, gPtr.Board)
			expand = false
		}

		if gPtr.GameOver {
			s := lo.GreedyScore(gPtr.Board)
			if m.side != gPtr.WhoseTurn {
				s *= -1
			}
			if s > 0 {
				wonOrLost = 1
			} else if s == 0 {
				wonOrLost = 0
			} else {
				wonOrLost = -1
			}
			break
		}

		legalsList, legalsLen := gPtr.LegalMoves()
		// Start with a random move; we have a chance to improve it if
		// we've been to this node before and seen all its children.
		nextMove := legalsList[rand.Intn(legalsLen)]
		// legalsLen > 1 -> there's a point to looking for another move.
		// expand -> the current node is not new, so it might have children we
		//   visited.
		if expand && legalsLen > 1 {
			seenAll := true
			visWins := make([]VisWin, legalsLen)
			totalVisits := 0
			for i, move := range legalsList {
				gNext := *gPtr
				gNext.ApplyMove(move)
				vw, ok := m.sta.VisWin(gNext.Board)
				if !ok {
					// We haven't seen all the next states so we can't
					// calculate all the UCBs and refine our move.
					seenAll = false
					break
				}
				visWins[i] = vw
				totalVisits += vw.visits
			}
			if seenAll {
				// We have seen all the next states; pick the move with the best UCB
				// UCB formula:
				// x_mov + √(C*ln(n_ttl) / n_mov)
				//   x is the "payoff" (win rate) for this move
				//   C is the "exploration constant"
				//   n_ttl is the total visits of the next states
				//   n_mov is the number of visits for this move
				// The term after the x will increase for less-visited moves
				// until it becomes greater than the win rate, at which point
				// we'll visit that move again.
				ucbFactor := m.ExploreConstant * math.Log(float64(totalVisits))
				bestUCB := math.Inf(-1)
				for i, move := range legalsList {
					vw := visWins[i]
					wr := vw.WinRate()
					if gPtr.WhoseTurn != m.side {
						// Assume opponent picks their best move
						wr *= -1
					}
					ucb := wr + math.Sqrt(ucbFactor/float64(vw.visits))
					if ucb > bestUCB {
						bestUCB = ucb
						nextMove = move
					}
				}
				// dprintf("move %v has best UCB: %f\n", nextMove, bestUCB)
			}
		}
		gPtr.ApplyMove(nextMove)
	}

	for _, turn := range visited {
		m.sta.BumpVisWin(turn, wonOrLost)
	}
}

// PickMove for UCTMover.
func (m *UCTMover) PickMove(gPtr *lo.Game, timeRemaining time.Duration) lo.Move {
	// won't need the table for this piece count once we're done
	// nor previous piece count (that one was probably played by the opponent)
	defer m.sta.Clear(gPtr.PieceCount)
	defer m.sta.Clear(gPtr.PieceCount - 1)

	legalsList, legalsLen := gPtr.LegalMoves()
	if legalsLen == 1 {
		return legalsList[0]
	}

	// Give ourselves a time limit for making a decision based on the
	// number of pieces we still have to place.
	timeForThisMove := time.Duration(int(timeRemaining) / (1 + (32 - (gPtr.PieceCount / 2))))
	dprintln("Move time:", timeForThisMove)
	m.outOfTime = false
	m.timeoutTimer = time.AfterFunc(timeForThisMove, func() {
		m.outOfTime = true
	})
	defer m.timeoutTimer.Stop()

	m.side = gPtr.WhoseTurn

	// Safety: try each legal move at least once.
	for _, move := range legalsList {
		gNext := *gPtr
		gNext.ApplyMove(move)
		m.simulate(&gNext)
	}

	r := m.Runs
	if m.IncreaseRuns {
		mult := 2.0 * float64(gPtr.PieceCount-3) / 30.0
		r = int(float64(r) * mult)
	}

	// Run simulations. Do m.runs runs for each legal move.
	for i := 1; i <= r; i++ {
		if m.Parallel {
			var wg sync.WaitGroup
			for j := 0; j < legalsLen; j++ {
				wg.Add(1)
				go func() {
					defer wg.Done()
					gCopy := *gPtr
					m.simulate(&gCopy)
					if m.outOfTime {
						return
					}
				}()
			}
			wg.Wait()
		} else {
			for j := 0; j < legalsLen; j++ {
				gCopy := *gPtr
				m.simulate(&gCopy)
				if m.outOfTime {
					break
				}
			}
		}
		if m.outOfTime {
			dprintln("out of time")
			break
		}
	}

	// Find all the moves we simulated.
	smlist := make([]lo.ScoredMove, legalsLen)
	for i, move := range legalsList {
		gNext := *gPtr
		gNext.ApplyMove(move)
		vw, ok := m.sta.VisWin(gNext.Board)
		if ok {
			score := lo.Score(100.0 * float64(vw.wins) / float64(vw.visits))
			dprintf("%v %d/%d -> %v\n", move, vw.wins, vw.visits, score)
			smlist[i] = lo.ScoredMove{Move: move, Score: score}
		} else {
			log.Fatalln(move, "never visited: this shouldn't have happened")
		}
	}

	best, _ := lo.PickBestMove(smlist)

	return best
}

// vim:ft=go:sts=8:sw=8:noet
