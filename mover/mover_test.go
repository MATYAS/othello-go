package mover

import lo "othello/libothello"
import "testing"

var emptyargsmap = make(map[string]string, 0)

// TestIDDFSCopy tests that the Copy function works for IDDFSMover.
func TestIDDFSCopy(t *testing.T) {
	m1 := IDDFSMover{}
	m1.Init(emptyargsmap)

	g := lo.NewGame()
	m1.tts.AddMove(&g, lo.Move(0x0000000400000000), lo.Score(0.1))
	// m1's table at this board state now has 1 move

	m1mts, m1ok := m1.tts.GetMoveMap(&g)
	if !m1ok {
		t.Error("no move map? we just added one")
	}
	if len(m1mts) != 1 {
		t.Error("len is wrong:", len(m1mts))
	}

	m2 := m1.Copy().(*IDDFSMover)

	// m2's table should have the same move that m1's does
	m2mts, m2ok := m2.tts.GetMoveMap(&g)
	if !m2ok {
		t.Error("no move map? we copied it")
	}
	if len(m2mts) != 1 {
		t.Error("len is wrong:", len(m2mts))
	}

	// add another move to m1's table. Check that it's there in m1...
	m1.tts.AddMove(&g, lo.Move(0x1), lo.Score(2.0))
	m1mts, m1ok = m1.tts.GetMoveMap(&g)
	if !m1ok {
		t.Error("no move map? we just added one")
	}
	if len(m1mts) != 2 {
		t.Error("len is wrong:", len(m1mts))
	}

	// ...but not in m2
	m2mts, m2ok = m2.tts.GetMoveMap(&g)
	if !m2ok {
		t.Error("no move map? we copied it")
	}
	if len(m2mts) != 1 {
		t.Error("len is wrong:", len(m2mts))
	}
}

// TestUCTCopy tests that the Copy function works for UCTMover.
func TestUCTCopy(t *testing.T) {
	m1 := UCTMover{}
	m1.Init(emptyargsmap)

	g := lo.NewGame()
	brd := g.Board
	m1.sta.SetVisWin(brd, VisWin{1, 1})

	m1vw, m1ok := m1.sta.VisWin(brd)
	if !m1ok {
		t.Error("we just added one")
	}
	if m1vw.visits != 1 || m1vw.wins != 1 {
		t.Error("values are wrong", m1vw)
	}

	m2 := m1.Copy().(*UCTMover)

	// m2's table should have the vw that m1's does
	m2vw, m2ok := m2.sta.VisWin(brd)
	if !m2ok {
		t.Error("what? we copied it")
	}
	if m2vw.visits != 1 || m2vw.wins != 1 {
		t.Error("values are wrong", m2vw)
	}

	// bump value; it should show up in m1...
	m1.sta.BumpVisWin(brd, 0)
	m1vw, m1ok = m1.sta.VisWin(brd)
	if !m1ok {
		t.Error("we just added one")
	}
	if m1vw.visits != 2 || m1vw.wins != 1 {
		t.Error("values are wrong", m1vw)
	}

	// ...but not in m2
	m2vw, m2ok = m2.sta.VisWin(brd)
	if !m2ok {
		t.Error("what? we copied it")
	}
	if m2vw.visits != 1 || m2vw.wins != 1 {
		t.Error("values are wrong", m2vw)
	}

}
