package mover

import (
	"math/rand"
	lo "othello/libothello"
	"runtime"
	"strconv"
	"sync"
	"time"
)

// MCTSMover implements naïve Monte Carlo Tree Search. Each legal move
// is evaluated `runs` times -- where "evaluation" means playing a
// random game until the end and tallying up outcomes +1, -1, 0 for win, loss,
// and tie respectively.
type MCTSMover struct {
	Runs         int
	Parallel     bool
	outOfTime    bool
	side         lo.Side
	timeoutTimer *time.Timer
}

// Init sets the number of simulations run for MCTSMover.
func (m *MCTSMover) Init(opts map[string]string) {
	m.Runs = 15000
	m.Parallel = runtime.GOMAXPROCS(0) > 1
	val, ok := opts["r"]
	if ok {
		var err error
		m.Runs, err = strconv.Atoi(val)
		if err != nil {
			panic("error: " + err.Error())
		}
		dprintln("mcts: set runs to", m.Runs)
	}
}

// Copy creates a copy of this mover.
func (m *MCTSMover) Copy() Mover {
	newm := *m
	return &newm
}

func (m *MCTSMover) simulate(gPtr *lo.Game) lo.Score {
	for {
		if gPtr.GameOver {
			greedy := lo.GreedyScore(gPtr.Board)
			if m.side != gPtr.WhoseTurn {
				greedy *= -1
			}
			if greedy > 0 {
				return lo.Score(1)
			} else if greedy < 0 {
				return lo.Score(-1)
			}
			return lo.Score(0)
		}

		legalsList, legalsLen := gPtr.LegalMoves()
		gPtr.ApplyMove(legalsList[rand.Intn(legalsLen)])
	}
}

// PickMove for MCTSMover.
func (m *MCTSMover) PickMove(gPtr *lo.Game, timeRemaining time.Duration) lo.Move {
	legalsList, legalsLen := gPtr.LegalMoves()

	if legalsLen == 1 {
		return legalsList[0]
	}

	// Give ourselves a time limit for making a decision based on the
	// number of pieces we still have to place.
	timeForThisMove := time.Duration(int(timeRemaining) / (1 + (32 - (gPtr.PieceCount / 2))))
	m.outOfTime = false
	m.timeoutTimer = time.AfterFunc(timeForThisMove, func() {
		m.outOfTime = true
	})
	defer m.timeoutTimer.Stop()

	m.side = gPtr.WhoseTurn

	helper := func(move lo.Move) lo.ScoredMove {
		var score lo.Score
		var i int

		for i = 1; i <= m.Runs; i++ {
			// simulate() will modify gNext so we do want to make a copy
			// each time.
			gNext := *gPtr
			gNext.ApplyMove(move)
			score += m.simulate(&gNext)
			if m.outOfTime {
				break
			}
		}
		score = lo.Score(m.Runs) * score / lo.Score(i)
		sm := lo.ScoredMove{Move: move, Score: score}
		dprintln(sm)

		return sm
	}

	smlist := make([]lo.ScoredMove, legalsLen)

	if m.Parallel {
		var wg sync.WaitGroup
		for i, move := range legalsList {
			wg.Add(1)
			go func(i int, move lo.Move) {
				smlist[i] = helper(move)
				wg.Done()
			}(i, move)
		}
		wg.Wait()
	} else {
		for i, move := range legalsList {
			smlist[i] = helper(move)
		}
	}

	best, _ := lo.PickBestMove(smlist)

	return best
}

// vim:ft=go:sts=8:sw=8:noet
