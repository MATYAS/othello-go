package mover

import (
	"math/bits"
	"math/rand"
	lo "othello/libothello"
	"strconv"
	"time"
)

// Transposition table for a minimax-type mover. There are multiple
// tables, keyed by piece count so the old ones can be deleted.

// MMXTransTableMaxDepth is the max depth of a transposition table from the
// current game state. 5 will _usually_ keep it under 2G.
var MMXTransTableMaxDepth = uint(5)

// MMXTransTable is a map of board positions to move/score mappings.
type MMXTransTable map[lo.Board]lo.MoveToScoreMap

// MMXTransTableSet is a hash-based implementation of a set of transposition
// tables for minimax, keyed by piece count. A transposition table
// is a map of board positions & sides to a map of moves to associated scores.
// 80% of the entries are going to be empty most of the time but
// performance is better than with a hash
type MMXTransTableSet [65]MMXTransTable

// NewMMXTransTableSet creates a new MMXTransTableSet
func NewMMXTransTableSet() MMXTransTableSet {
	tts := MMXTransTableSet{}
	for i := range tts {
		tts[i] = make(MMXTransTable, 0)
	}
	return tts
}

// Clear deletes an unnecessary table.
func (tts *MMXTransTableSet) Clear(pieceCount int) {
	tts[pieceCount] = make(MMXTransTable, 0)
}

// AddMove adds a move to a table or updates its score.
func (tts *MMXTransTableSet) AddMove(g *lo.Game, m lo.Move, s lo.Score) {
	tt := tts[g.PieceCount]
	mts, ok := tt[g.Board]
	if !ok {
		tt[g.Board] = make(lo.MoveToScoreMap, 0)
		mts = tt[g.Board]
	}
	mts[m] = s
}

// SetTableEntry sets the MoveToScoreMap for a board in a table.
func (tts *MMXTransTableSet) SetTableEntry(g *lo.Game, mts lo.MoveToScoreMap) {
	tt := tts[g.PieceCount]
	tt[g.Board] = mts
}

// GetMoveMap returns the map of moves to scores for a board.
func (tts *MMXTransTableSet) GetMoveMap(g *lo.Game) (mts lo.MoveToScoreMap, ok bool) {
	tt := tts[g.PieceCount]
	mts, ok = tt[g.Board]
	if !ok {
		return make(lo.MoveToScoreMap, 0), false
	}
	return mts, true
}

/***************************************************************************
* Iterative Deepening Depth-First Search mover
***************************************************************************/

const endGamePC = 48

// IDDFSMover uses the minimax algorithm with iterative deepening.
type IDDFSMover struct {
	Depth              uint
	Steady             uint
	NonLinearTimeout   bool
	ConserveTime       bool
	AdvancedTimeBudget bool
	MobilityBonus      float64
	TableDepth         uint // really recommended not to override this except for testing
	tts                MMXTransTableSet
	outOfTime          bool
	timeoutTimer       *time.Timer
}

// Init sets the max depth for IDDFSMover.
func (m *IDDFSMover) Init(opts map[string]string) {
	m.Depth = 7
	m.TableDepth = MMXTransTableMaxDepth
	m.tts = NewMMXTransTableSet()
	m.NonLinearTimeout = false
	m.ConserveTime = true
	m.AdvancedTimeBudget = true
	m.MobilityBonus = 0.5
	var val string
	var ok bool
	val, ok = opts["d"]
	if ok {
		tmp, err := strconv.ParseUint(val, 10, 64)
		if err != nil {
			panic("error: " + err.Error())
		}
		m.Depth = uint(tmp)
		dprintln("iddfs: set depth to", m.Depth)
	}
	val, ok = opts["steady"]
	if ok {
		tmp, err := strconv.Atoi(val)
		if err != nil {
			panic("error: " + err.Error())
		}
		if tmp > 0 {
			m.Steady = uint(tmp)
		} else {
			m.Steady = uint(int(m.Depth) + tmp)
		}
		dprintln("iddfs: set steady to", m.Steady)
	} else {
		m.Steady = m.Depth
	}
	val, ok = opts["nlt"]
	if ok {
		tmp, err := strconv.Atoi(val)
		if err != nil {
			panic("error: " + err.Error())
		}
		if tmp > 0 {
			m.NonLinearTimeout = true
		} else {
			m.NonLinearTimeout = false
		}
		dprintln("iddfs: set nonLinearTimeout to", m.NonLinearTimeout)
	}
	val, ok = opts["cons"]
	if ok {
		tmp, err := strconv.Atoi(val)
		if err != nil {
			panic("error: " + err.Error())
		}
		if tmp > 0 {
			m.ConserveTime = true
		} else {
			m.ConserveTime = false
		}
		dprintln("iddfs: set conserveTime to", m.ConserveTime)
	}
	val, ok = opts["atb"]
	if ok {
		tmp, err := strconv.Atoi(val)
		if err != nil {
			panic("error: " + err.Error())
		}
		if tmp > 0 {
			m.AdvancedTimeBudget = true
		} else {
			m.AdvancedTimeBudget = false
		}
		dprintln("iddfs: set advancedTimeBudget to", m.AdvancedTimeBudget)
	}
	val, ok = opts["mob"]
	if ok {
		var err error
		m.MobilityBonus, err = strconv.ParseFloat(val, 64)
		if err != nil {
			panic("error: " + err.Error())
		}
		dprintln("iddfs: set MobilityBonus to", m.MobilityBonus)
	}
}

// Copy creates a copy of this mover with its own TTS.
func (m *IDDFSMover) Copy() Mover {
	newm := *m
	newm.tts = NewMMXTransTableSet()
	// An MMXTransTableSet is an array of maps; first iterate over the array.
	for i := range m.tts {
		for k, v := range m.tts[i] {
			// k is a Board and v is a MoveToScoreMap; we need to copy v as well.
			vcopy := make(lo.MoveToScoreMap, len(v))
			for j, v2 := range v {
				vcopy[j] = v2
			}
			newm.tts[i][k] = vcopy
		}
	}
	return &newm
}

// alpha-beta pruning from
//   Stuart Russel and Peter Norvig. "Alpha-Beta Pruning" in _Artificial Intelligence: A Modern Approach_ 2nd ed.
//     Upper Saddle River, NJ: Pearson Education, 2003, pp. 167-171.
func (m *IDDFSMover) minimax(depth, maxdepth uint, gPtr *lo.Game, min bool, alpha, beta lo.Score) lo.Score {
	if gPtr.GameOver {
		return 1024.0 * lo.GreedyScore(gPtr.Board)
	}
	legals, legalsLen := gPtr.LegalMoves()
	if depth >= maxdepth {
		mob := float64(len(legals)) * m.MobilityBonus
		if min {
			mob *= -1
		}
		return lo.WeightedScore(gPtr.Board) + lo.Score(mob)
	}

	bestScore := lo.LowestScore
	if min {
		bestScore = lo.HighestScore
	}

	// If we only have one legal move, exploring an additional level doesn't
	// widen the tree.
	if legalsLen == 1 {
		maxdepth++
	}

	// Try to get the list of moves sorted best-first
	movemap, ok := m.tts.GetMoveMap(gPtr)
	// Augment the cached moves in the move map with the ones we haven't seen yet
	for _, move := range legals {
		_, ok = movemap[move]
		if !ok {
			movemap[move] = bestScore
		}
	}

	var smlist []lo.ScoredMove
	if min {
		// for a min node, getting the lowest score first will let us prune the most
		smlist = movemap.GetSortedList()
	} else {
		// for a max node, getting the highest score first will let us prune the most
		smlist = movemap.GetReverseSortedList()
	}
	for _, sm := range smlist {
		move := sm.Move
		// dprintAlphaBeta(depth, move, min, alpha, beta)
		gNext := *gPtr
		/*
					if !gNext.IsLegalMove(move) {
						dprintf(`!!! ILLEGAL MOVE ATTEMPT
			Depth %d / %d; pieces: %d; smlist:
			%v
			`, depth, maxdepth, gPtr.PieceCount, smlist)
					}
		*/
		gNext.ApplyMove(move)
		score := m.minimax(depth+1, maxdepth, &gNext, !min, alpha, beta)
		// Ignore the best score since our evaluation is incomplete
		if m.outOfTime {
			return lo.Score(0)
		}

		if depth <= m.TableDepth {
			m.tts.AddMove(gPtr, move, score)
		}
		// dprintfIndent(depth+1, "%-3s %4d\n", move, score)

		if (min && score < bestScore) || (!min && score > bestScore) {
			bestScore = score
		}
		if min {
			if bestScore <= alpha {
				// dprintfIndent(depth+1, "    ⬇︎ pruning (⍺ >= %4d)\n", bestScore)
				return bestScore
			}
			beta = lo.ScoreMin(beta, bestScore)
		} else {
			if bestScore >= beta {
				// dprintfIndent(depth+1, "    ⬆︎ pruning (β <= %4d)\n", bestScore)
				return bestScore
			}
			alpha = lo.ScoreMax(alpha, bestScore)
		}
	}
	return bestScore
}

// PickMove for IDDFSMover.
func (m *IDDFSMover) PickMove(gPtr *lo.Game, timeRemaining time.Duration) lo.Move {
	// won't need the table for this piece count once we're dont
	// nor previous piece count (that one was probably played by the opponent)
	defer m.tts.Clear(gPtr.PieceCount)
	defer m.tts.Clear(gPtr.PieceCount - 1)

	calcStart := time.Now()

	legalsList, legalsLen := gPtr.LegalMoves()

	if legalsLen == 1 {
		return legalsList[0]
	}

	// Give ourselves a time limit for making a decision based on the
	// number of pieces we still have to place.
	var timeForThisMove time.Duration
	if m.AdvancedTimeBudget && gPtr.PieceCount <= 18 {
		// Try to use up 10% of the time before piece 18.
		timeDivisor := (18 - gPtr.PieceCount) / 2
		if timeDivisor < 1 {
			timeDivisor = 1
		}
		timeForThisMove = time.Duration(int(10*timeRemaining/100) / timeDivisor)
	} else if m.AdvancedTimeBudget && gPtr.PieceCount <= 44 {
		// Try to use up 85% of the time before piece 44.
		timeDivisor := (44 - gPtr.PieceCount) / 2
		if timeDivisor < 1 {
			timeDivisor = 1
		}
		timeForThisMove = time.Duration(int(85*timeRemaining/100) / timeDivisor)
	} else if gPtr.PieceCount <= 62 {
		// The endgame goes pretty quickly
		timeDivisor := (60 - gPtr.PieceCount) / 2
		if timeDivisor < 1 {
			timeDivisor = 1
		}
		timeForThisMove = time.Duration(int(timeRemaining) / timeDivisor)
	} else {
		// The last moves are basically instantaneous.
		timeForThisMove = timeRemaining
	}

	// don't do the nlt calculation if we have fewer than 5 seconds
	if m.NonLinearTimeout && timeForThisMove > time.Duration(5000000000) {
		// Give us more time based on number of legal moves (logarithmic).
		factor := 64 - bits.LeadingZeros(uint(legalsLen))
		if factor < 1 {
			factor = 1
		}
		timeForThisMove = time.Duration(int(timeForThisMove) * factor)
	}

	// Safety
	timeCutoff := 80 * (timeRemaining - time.Now().Sub(calcStart)) / 100
	if timeForThisMove > timeCutoff {
		timeForThisMove = timeCutoff
	}

	dprintln("Move time:", timeForThisMove)

	// make sure bestMove is always legal even if we don't finish any iterations
	bestMove := legalsList[0]

	// If we're really short on time, use a circuit breaker that stops
	// computation immediately.
	if timeForThisMove < time.Duration(1000000) {
		// if we really have no time, (< 1 ms)
		// make a move so we don't forfeit.
		dprint("we're doomed! ")
		return legalsList[0]
	}
	m.outOfTime = false
	startTime, endTime := time.Now(), time.Now().Add(timeForThisMove)
	m.timeoutTimer = time.AfterFunc(timeForThisMove, func() {
		m.outOfTime = true
	})
	defer m.timeoutTimer.Stop()

	maxdepth, steady, conserveTime := m.Depth, m.Steady, m.ConserveTime
	if gPtr.PieceCount >= endGamePC {
		// search until the end; no steadiness shortcut
		maxdepth = uint(64 - gPtr.PieceCount)
		steady = maxdepth
		conserveTime = false
	}

	var streak uint
	for iterdepth := uint(0); iterdepth <= maxdepth; iterdepth++ {
		alpha := lo.LowestScore
		bestScore := lo.LowestScore

		// Try to get the list of moves sorted best-first
		movemap, _ := m.tts.GetMoveMap(gPtr)
		// Augment the cached moves in the move map with the ones we haven't seen yet
		// (in random order)
		for _, moveIdx := range rand.Perm(legalsLen) {
			move := legalsList[moveIdx]
			if _, ok := movemap[move]; !ok {
				movemap[move] = lo.LowestScore
			}
		}

		smlist := movemap.GetSortedList()
		newBestMove := bestMove
		// dprintln(smlist)
		for _, sm := range smlist {
			move := sm.Move
			gNext := *gPtr
			gNext.ApplyMove(move)
			// dprintf("\n%-3s⬆︎\n", move)
			score := m.minimax(0, iterdepth, &gNext, true, alpha, lo.HighestScore)
			if m.outOfTime {
				dprintln("out of time at", iterdepth)
				// not newBestMove! our evaluation is incomplete.
				return bestMove
			}
			m.tts.AddMove(gPtr, move, score)
			if score > bestScore {
				newBestMove, bestScore = move, score
			}
			// dprintf("%-3s⬆︎%4d\n", move, score)

			// This is a "max" node, so beta never gets updated.
			alpha = lo.ScoreMax(alpha, bestScore)
		}
		// elapsed for all iters so far
		timeElapsed := time.Now().Sub(startTime)
		// "Steadiness": if a move has a good enough winning streak (been
		// the best move for enough iterations), stop and return it.
		// This may lead to suboptimal moves (I've seen 9-win streaks broken)
		// so this is just another way to favor speed over correctness.
		if bestMove == newBestMove {
			streak++
			if streak >= steady {
				dprintln("move chosen after streak of", streak)
				return bestMove
			}
		} else {
			bestMove = newBestMove
			if streak > 4 {
				dprintln("streak broken after", streak)
			}
			streak = 0
		}

		// It takes ~4-5 times as long for each iteration; don't
		// start the next one if we won't have the time to finish it.
		if conserveTime && timeElapsed*4 > endTime.Sub(time.Now()) && iterdepth < maxdepth {
			dprintf("iter %d done; won't have time for next\n", iterdepth)
			return bestMove
		}
	}

	return bestMove
}

// Clear clears the entire TTS to free up memory.
func (m *IDDFSMover) Clear() {
	m.tts = NewMMXTransTableSet()
}
