package mover

import (
	"fmt"
	lo "othello/libothello"
	"strings"
	"time"
)

// HumanMover asks for a move on the console.
type HumanMover struct{}

// Init does nothing.
func (*HumanMover) Init(map[string]string) {}

// Copy creates a copy of this mover.
func (*HumanMover) Copy() Mover { return &HumanMover{} }

// PickMove for HumanPlayer.
func (*HumanMover) PickMove(gPtr *lo.Game, timeRemaining time.Duration) lo.Move {
	legals := gPtr.Legals()

	var pos lo.Pos
	for {
		fmt.Printf("next move for %v ====> ", gPtr.WhoseTurn)
		var input string
		fmt.Scanln(&input)
		input = strings.ToUpper(strings.TrimSpace(input))
		if len(input) < 1 {
			continue
		}
		if input[0] == 'P' {
			if legals > 0 {
				fmt.Println("YOU SHALL NOT PASS!")
				continue
			}
			return lo.PassingMove
		}
		if len(input) == 2 {
			pos.X = uint(input[0] - 'A')
			pos.Y = uint(input[1] - '1')
		} else if len(input) == 3 && input[1] == ',' {
			// coords-based input is row-major
			pos.Y = uint(input[0] - '0')
			pos.X = uint(input[2] - '0')
		} else {
			fmt.Println("invalid input format")
			continue
		}
		// can't be < 0 b/c they're unsigned
		if pos.X > 7 || pos.Y > 7 {
			fmt.Println("input out of range")
			continue
		}

		move := lo.Move(pos.ToBitmap())
		if uint64(move)&uint64(legals) == 0 {
			fmt.Println("invalid move")
			continue
		}
		return move
	}
}

// vim:ft=go:sts=8:sw=8:noet
