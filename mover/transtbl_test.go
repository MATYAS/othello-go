package mover

import (
	"fmt"
	lo "othello/libothello"
	"testing"
	"time"
)

// genGameWithPieceCount returns a game with the given piece count
// after playing randomly.
func genGameWithPieceCount(pc int) (g lo.Game, err error) {
	var player, opponent MonkeyMover
	var move lo.Move

	if pc < 4 || pc > 64 {
		return lo.Game{}, fmt.Errorf("piece count %d out of range", pc)
	}
	for {
		g = lo.NewGame()
		for !g.GameOver && g.PieceCount < pc {
			move, player, opponent = player.PickMove(&g, time.Duration(10*time.Minute)), opponent, player
			g.ApplyMove(move)
		}
		if g.PieceCount == pc {
			return g, nil
		}
	}
}

func TestMMTTSOps(t *testing.T) {
	ts := NewMMXTransTableSet()

	for i := 4; i <= 64; i++ {
		g, err := genGameWithPieceCount(i)
		if err != nil {
			t.Fatalf("genGameWithPieceCount(%d) failed", i)
		}
		ll, llen := g.LegalMoves()
		m := ll[0]
		s := lo.Score(1)
		ts.AddMove(&g, m, s)
		for j := 1; j < llen; j++ {
			ts.AddMove(&g, ll[j], 0)
		}

		movemap, _ := ts.GetMoveMap(&g)
		smlist := movemap.GetReverseSortedList()
		if smlist[0].Score != lo.Score(1) {
			t.Errorf("Expected score not found")
		}
		if len(smlist) != llen {
			t.Errorf("Score list length unexpected (want: %d found %d)", llen, len(smlist))
		}
		s = lo.HighestScore
		for i := range smlist {
			if s < smlist[i].Score {
				t.Errorf("Scores not sorted (%v < %v)", s, smlist[i].Score)
			}
			s = smlist[i].Score
		}
		ts.Clear(i - 1)
		if l := len(ts[i-1]); l != 0 {
			t.Errorf("delete of %d failed: length is %d", i-1, l)
		}
	}
}
