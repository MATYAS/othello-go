package mover

import (
	"math/rand"
	lo "othello/libothello"
	"time"
)

///////////////////////////////////////////////////////////////////////
// MonkeyMover
///////////////////////////////////////////////////////////////////////

// MonkeyMover picks a legal move at random.
type MonkeyMover struct{}

// Init does nothing.
func (*MonkeyMover) Init(map[string]string) {}

// Copy creates a copy of this mover.
func (*MonkeyMover) Copy() Mover { return &MonkeyMover{} }

// PickMove for MonkeyPlayer.
func (*MonkeyMover) PickMove(gPtr *lo.Game, timeRemaining time.Duration) lo.Move {
	legalsList, legalsLen := gPtr.LegalMoves()
	return legalsList[rand.Intn(legalsLen)]
}

///////////////////////////////////////////////////////////////////////
// GreedyMover
///////////////////////////////////////////////////////////////////////

// GreedyMover picks one of the moves that will immediately get it the
// most pieces.
type GreedyMover struct{}

// Init does nothing.
func (*GreedyMover) Init(map[string]string) {}

// Copy creates a copy of this mover.
func (*GreedyMover) Copy() Mover { return &GreedyMover{} }

// PickMove for GreedyPlayer
func (*GreedyMover) PickMove(gPtr *lo.Game, timeRemaining time.Duration) lo.Move {
	var smlist []lo.ScoredMove
	legalsList, _ := gPtr.LegalMoves()

	for _, move := range legalsList {
		smlist = append(smlist, lo.ScoredMove{Move: move,
			Score: lo.Score(lo.Bitcount(gPtr.Flips(move)))})
	}

	best, _ := lo.PickBestMove(smlist)

	return best
}

///////////////////////////////////////////////////////////////////////
// WeightedMover
///////////////////////////////////////////////////////////////////////

// WeightedMover is like GreedyPlayer but applies weights to positions.
type WeightedMover struct{}

// Init does nothing.
func (*WeightedMover) Init(map[string]string) {}

// Copy creates a copy of this mover.
func (*WeightedMover) Copy() Mover { return &WeightedMover{} }

// PickMove for WeightedMover
func (m *WeightedMover) PickMove(gPtr *lo.Game, timeRemaining time.Duration) lo.Move {
	var smlist []lo.ScoredMove
	legalsList, _ := gPtr.LegalMoves()

	for _, move := range legalsList {
		gNext := *gPtr
		gNext.ApplyMove(move)
		// -1 because after the ApplyMove, positive is better for the opponent.
		score := -1.0 * lo.WeightedScore(gNext.Board)
		smlist = append(smlist, lo.ScoredMove{Move: move, Score: score})
	}

	best, bestScore := lo.PickBestMove(smlist)

	dprintln("best:", best, bestScore)
	return best
}
