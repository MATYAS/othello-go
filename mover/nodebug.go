// +build !debug

// Disable debug logging
// trick from https://dave.cheney.net/2014/09/28/using-build-to-switch-between-debug-and-release

package mover

import lo "othello/libothello"

func dprintf(fmt string, args ...interface{}) {}

func dprint(args ...interface{}) {}

func dprintln(args ...interface{}) {}

func indent(uint) string { return "" }

func dprintfIndent(indent uint, f string, args ...interface{}) {}

func dprintAlphaBeta(depth uint, move lo.Move, min bool, alpha, beta lo.Score) {}
