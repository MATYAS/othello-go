#!/usr/bin/make -f

all: othello_darwin othello_linux othello

othello_darwin: *.go libothello/*.go
	GOOS=darwin go build -o $@

othello_linux: *.go libothello/*.go
	GOOS=linux go build -o $@

othello: *.go libothello/*.go
	go build -o $@

.PHONY: all
