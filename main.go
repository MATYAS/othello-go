package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	lo "othello/libothello"
	om "othello/mover"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"time"
)

var aggressiveGC bool
var speculate bool

// tourneyStats contains the statistics for the tournament as a whole.
// This is updated after every match and used to print the table at the end.
type tourneyStats struct {
	PlayerNames   [2]string
	Wins          [2]uint
	Perfect       [2]uint
	Forfeits      [2]uint
	PeakTimeTaken [2]time.Duration
	Ties          uint
	MatchesPlayed uint
}

// ghost is a player that uses the opponent's mover to predict moves for
// themselves when speculation is on.
type ghost struct {
	name    string
	mover   om.Mover
	ok      bool
	spechit int
	spectot int
}

// Match represents the state of an entire match, including the game state,
// the players, parameters, time taken, etc.
type Match struct {
	Game       *lo.Game
	Players    [2]*Player
	PieceCount [2]int
	TimeTaken  [2]time.Duration
	TimeLimit  time.Duration
	forfeited  [2]bool
	// ghosts[Black] is a copy of White's Mover predicting what Black will play.
	ghosts [2]*ghost
}

// NewMatch returns a match for a new game.
func NewMatch(players [2]*Player, timeLimit time.Duration) Match {
	g := lo.NewGame()
	var ghosts [2]*ghost
	// Create a ghost player for White that
	// predicts moves for White using Black's mover, and vice versa, if
	// speculation is on.
	for p := lo.Black; p <= lo.White; p++ {
		o := p.Other()
		pMover := players[p].Mover
		_, pIsHuman := (pMover).(*om.HumanMover)
		if speculate && !pIsHuman { // humans don't speculate (I mean, you can if you want to)
			ghosts[o] = &ghost{mover: pMover.Copy(), ok: true, name: players[p].Name}
		} else {
			ghosts[o] = &ghost{ok: false}
		}
	}

	return Match{Game: &g, Players: players, ghosts: ghosts, TimeLimit: timeLimit}
}

func (ts *tourneyStats) update(m *Match) {
	ts.MatchesPlayed++

	for p := 0; p <= 1; p++ {
		timeTaken := m.TimeTaken[p]
		if m.forfeited[p] {
			timeTaken = m.TimeLimit
		}
		if timeTaken > ts.PeakTimeTaken[p] {
			ts.PeakTimeTaken[p] = timeTaken
		}
	}
	for p := 0; p <= 1; p++ {
		o := 1 - p

		if m.forfeited[p] && !m.forfeited[o] {
			ts.Forfeits[p]++
			ts.Wins[o]++
			break
		}
		if m.PieceCount[p] == m.PieceCount[o] {
			ts.Ties++
			break
		}
		if m.PieceCount[p] > m.PieceCount[o] {
			ts.Wins[p]++
			if m.PieceCount[o] == 0 {
				ts.Perfect[p]++
			}
			break
		}
	}
}

func (ts *tourneyStats) print() {
	percent := func(val uint) float32 {
		if ts.MatchesPlayed > 0 {
			return 100.0 * float32(val) / float32(ts.MatchesPlayed)
		}
		return float32(0.0)
	}

	fmt.Printf("Of %d games:\n\n", ts.MatchesPlayed)
	fmt.Printf(" Player        | Wins        | Forfeits | Perfect | Peak time\n")
	fmt.Printf("---------------+-------------+----------+---------+-----------\n")
	for p := 0; p <= 1; p++ {
		fmt.Printf(" %-12s  | %6d %3.f%% | %8d | %7d | %8.2fs\n",
			strings.Title(ts.PlayerNames[p]),
			ts.Wins[p], percent(ts.Wins[p]),
			ts.Forfeits[p],
			ts.Perfect[p],
			ts.PeakTimeTaken[p].Seconds())
	}
	fmt.Printf(" Ties          | %6d %3.f%% | -------- | ------- | ---------\n",
		ts.Ties, percent(ts.Ties))
}

// Player is one of the players, with a Name and a Mover and options for
// that mover (MoverOpts).
type Player struct {
	Name      string
	Mover     om.Mover
	MoverOpts map[string]string
}

func (m *Match) run(con ConsoleOptions) {
	game := m.Game
	var move, specmove lo.Move
	var specok bool

	for p := game.WhoseTurn; !game.GameOver; p = p.Other() {
		if !con.resultsOnly {
			con.PrintGame(game)
		}
		ply := m.Players[p]
		gho := m.ghosts[p]
		timeRemaining := m.TimeLimit - m.TimeTaken[p]
		specok = false
		if gho.ok {
			_, legalsLen := game.LegalMoves()
			if legalsLen > 1 { // only speculate if we have more than one move (else it's boring)
				// Speculation is on and current player not human: have a copy of
				// the opponent's Mover predict their move.
				fmt.Print("Opponent speculating...")
				dprintln("***", gho.name, "speculating;", timeRemaining, "left")
				specmove = gho.mover.PickMove(game, timeRemaining)
				fmt.Println("done")
				specok = true
			} else {
				dprintln("not speculating (only 1 move)")
			}
		}
		mover := &ply.Mover
		_, isHuman := (*mover).(*om.HumanMover)
		if !isHuman && !con.resultsOnly {
			fmt.Print("Picking move...")
		}
		dprintln("***", ply.Name, "picking;", timeRemaining, "left")
		startTime := time.Now()
		move = (*mover).PickMove(game, timeRemaining)
		endTime := time.Now()
		dur := endTime.Sub(startTime)
		if isHuman {
			// Count human time taken as 1/3. Humans can't run out of time
			// so this is just used for speculation.
			m.TimeTaken[p] += dur / 3
		} else {
			m.TimeTaken[p] += dur
		}
		if aggressiveGC {
			dprintf("GC-ing... ")
			runtime.GC()
			debug.FreeOSMemory()
			dprintf("done\n")
		}
		if !con.resultsOnly {
			if !isHuman && !con.nopause {
				fmt.Printf("done in %10v (tot: %10v). Hit enter.\n", dur, m.TimeTaken[p])
				fmt.Scanln()
			}
			// print the move before applying so we get who moved correct
			con.PrintMove(move, game.WhoseTurn)
			if specok {
				fmt.Printf(" -- %v speculated %v", gho.name, specmove)
				gho.spectot++
				if move == specmove {
					gho.spechit++
				}
				fmt.Printf(" %d/%d", gho.spechit, gho.spectot)
			}
			fmt.Println()
		}
		game.ApplyMove(move)

		m.PieceCount[lo.Black], m.PieceCount[lo.White] = game.PieceCounts()
		if m.TimeLimit > 0 && m.TimeTaken[p] > m.TimeLimit && !isHuman {
			// TurnCount-1 b/c TurnCount incremented when we did ApplyMove
			fmt.Printf("Turn %d: %s took too long (%v > %v)!\n", game.TurnCount-1, strings.Title(ply.Name), m.TimeTaken[p], m.TimeLimit)
			m.forfeited[p] = true
			game.GameOver = true
		}
	}
}

func (m *Match) writeStats(statsfp *os.File) {
	line := fmt.Sprintf("%s\t%d\t%d\t", m.Players[lo.Black].Name, m.PieceCount[lo.Black], m.TimeLimit-m.TimeTaken[lo.Black])
	line += fmt.Sprintf("%s\t%d\t%d", m.Players[lo.White].Name, m.PieceCount[lo.White], m.TimeLimit-m.TimeTaken[lo.White])

	if _, err := fmt.Fprintln(statsfp, line); err != nil {
		log.Fatal(err)
	}
}

// parseArgs parses the command line.
func parseArgs() ([2]*Player, ConsoleOptions, int, bool, time.Duration, time.Duration, string) {
	var con ConsoleOptions
	var keepfirst bool
	var matches int
	var timeLimitStr, tourneyTimeStr string
	var timeLimit, tourneyTime time.Duration
	var statsfname string
	var playerArgs [2]struct {
		moverStr     string
		moverRawOpts string
	}

	var players [2]*Player
	players[lo.Black] = new(Player)
	players[lo.White] = new(Player)

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Fprintf(os.Stderr,
			`Options for "minimax", "iddfs", and "hybrid" movers:
	d=<DEPTH>           Lookahead depth (default 7)
Options for "mcts", "uct", and "hybrid" movers:
	r=<RUNS>            Number of simulations to run (default 15000)
Options for "iddfs" and "hybrid" movers:
	steady=<INT>        Accept winning move after streak of this length;
	                    if negative, interpret as difference from depth.
	                    Default is off.
	atb=<0|1>           Advanced time budgeting: budget time based on game
	                    phase (early/mid/late). Default on.
	cons=<0|1>          Conserve time: don't start an iteration we don't
	                    think we'll finish. Default on.
	nlt=<0|1>           Non-linear time: give more time if we have more
	                    immediate moves to consider (logarithmic).
	                    Default off.
	mob=<FLOAT>         Mobility bonus: increase board score by mob for each
	                    free move on the board.
	                    Default 0.5.
Options for "uct" and "hybrid" movers:
	C=<FLOAT>           Exploration Constant
	ir=<0|1>            Increase runs as we get later in the game (default off)
Options for "hybrid" movers:
	change=<INT>        Change movers from IDDFS to UCT at this piece count
	                    (default 20)
`)
	}

	flag.BoolVar(&con.coords, "coords", false, "Display array coordinates")
	flag.BoolVar(&con.bow, "bow", false, "Use colors/symbols for light (black-on-white) terminal")
	flag.BoolVar(&con.rotate, "rotate", false, "Display board rotated 180")
	flag.BoolVar(&con.nocolor, "nocolor", false, "Do not use colors")
	flag.BoolVar(&con.nopause, "nopause", false, "Do not pause after non-human moves")
	flag.BoolVar(&con.notable, "notable", false, "Do not print table at the end")
	flag.BoolVar(&keepfirst, "keepfirst", false, "Do not switch first player every game")
	flag.BoolVar(&aggressiveGC, "aggressive-gc", false, "Garbage collect after every move (default false; recommended for batch runs)")
	flag.UintVar(&om.MMXTransTableMaxDepth, "table-depth", 5, "Max depth of transposition table (default 5; recommend decreasing for batch runs)")
	flag.IntVar(&matches, "matches", 1, "Play this many matches")
	for p := lo.Black; p <= lo.White; p++ {
		player := players[p]
		side := strings.ToLower(lo.Side(p).String())
		flag.StringVar(&playerArgs[p].moverStr, side, "human", side+" mover [human, monkey, greedy, weighted, minimax, mcts, iddfs, uct, hybrid]")
		flag.StringVar(&playerArgs[p].moverRawOpts, side+"opts", "", "Options for "+side+" mover")
		flag.StringVar(&player.Name, side+"name", strings.Title(side), "Name for "+side)
	}
	flag.StringVar(&timeLimitStr, "timelimit", "10m", "Max time per player")
	flag.StringVar(&tourneyTimeStr, "tourneytime", "48h", "Do not start new games after this time")
	flag.StringVar(&statsfname, "stats", "", "File to append statistics to")
	flag.BoolVar(&speculate, "speculate", false, "Print which move mover thinks opponent will take (default false; no effect if matches > 1 or both players are human)")
	flag.Parse()

	if matches < 1 {
		fmt.Println("Invalid count")
		os.Exit(2)
	}
	var err error
	timeLimit, err = time.ParseDuration(timeLimitStr)
	if err != nil || timeLimit < time.Duration(0) {
		fmt.Println("Invalid timelimit")
		os.Exit(2)
	}
	tourneyTime, err = time.ParseDuration(tourneyTimeStr)
	if err != nil || tourneyTime < time.Duration(0) {
		fmt.Println("Invalid tourneytime")
		os.Exit(2)
	}

	splitOpts := func(optarr []string) map[string]string {
		optmap := make(map[string]string)

		for _, val := range optarr {
			if val == "" {
				break
			}
			split := strings.SplitN(val, "=", 2)
			if len(split) == 1 {
				optmap[split[0]] = "1"
			} else {
				optmap[split[0]] = split[1]
			}
		}

		return optmap
	}

	for i := range players {
		ply := players[i]
		pa := playerArgs[i]
		switch pa.moverStr {
		case "human":
			ply.Mover = new(om.HumanMover)
		case "monkey":
			ply.Mover = new(om.MonkeyMover)
		case "greedy":
			ply.Mover = new(om.GreedyMover)
		case "weighted":
			ply.Mover = new(om.WeightedMover)
		case "minimax":
			ply.Mover = new(om.MinimaxMover)
		case "mcts":
			ply.Mover = new(om.MCTSMover)
		case "iddfs":
			ply.Mover = new(om.IDDFSMover)
		case "uct":
			ply.Mover = new(om.UCTMover)
		case "hybrid":
			ply.Mover = new(om.HybridMover)
		default:
			fmt.Printf("Invalid %s player type %s", ply.Name, pa.moverStr)
			os.Exit(2)
		}
		ply.MoverOpts = splitOpts(strings.Split(pa.moverRawOpts, ","))
	}

	con.resultsOnly = matches > 1

	if con.resultsOnly {
		speculate = false
	}

	return players, con, matches, keepfirst, timeLimit, tourneyTime, statsfname
}

func main() {
	players, con, matches, keepfirst, timeLimit, tourneyTime, statsfname := parseArgs()

	var err error
	var statsfp *os.File

	if statsfname != "" {
		statsfp, err = os.OpenFile(statsfname, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
		if err != nil {
			log.Fatal(err)
		}
		info, err := statsfp.Stat()
		if err != nil {
			log.Fatal(err)
		}
		if 0 == info.Size() {
			// we just created this - write a header
			if _, err := fmt.Fprintln(statsfp, "BName\tBPieces\tBTimeLeft\tWName\tWPieces\tWTimeLeft"); err != nil {
				log.Fatal(err)
			}
		}
	}

	rand.Seed(time.Now().UnixNano())

	ts := tourneyStats{}
	ts.PlayerNames[0] = players[0].Name
	ts.PlayerNames[1] = players[1].Name
	var columns int
	columns, err = strconv.Atoi(os.Getenv("COLUMNS"))
	if err != nil || columns < 1 {
		columns = 80
	}
	dot := float64(columns) / float64(matches)
	dotCounter := 0.0
	blackWinsPerDot, whiteWinsPerDot := 0, 0
	tourneyEnd := time.Now().Add(tourneyTime)
	for matchnum := 1; matchnum <= matches; matchnum++ {
		players[lo.Black].Mover.Init(players[lo.Black].MoverOpts)
		players[lo.White].Mover.Init(players[lo.White].MoverOpts)
		dprintln("------------------------------------------------")
		dprintln("                   NEW MATCH!")
		dprintln("                  match", matchnum, timeLimit)
		dprintln("------------------------------------------------")
		m := NewMatch(players, timeLimit)
		g := m.Game

		if !keepfirst {
			// a stupid trick to get the players to switch sides when playing
			// multiple games: have white go first on every other game.
			g.WhoseTurn = lo.Side((matchnum - 1) % 2)
		}
		debug.FreeOSMemory()
		m.run(con)
		ts.update(&m)
		if matches == 1 {
			con.PrintGame(g)
		} else {
			if m.PieceCount[lo.Black] > m.PieceCount[lo.White] {
				blackWinsPerDot++
			} else if m.PieceCount[lo.White] > m.PieceCount[lo.Black] {
				whiteWinsPerDot++
			}
			dotCounter += dot
			if dotCounter >= 1.0 {
				dotCounter -= 1.0
				if blackWinsPerDot > whiteWinsPerDot {
					fmt.Print("b")
				} else if whiteWinsPerDot > blackWinsPerDot {
					fmt.Print("w")
				} else {
					fmt.Print(".")
				}
				blackWinsPerDot, whiteWinsPerDot = 0, 0
			}
		}
		if statsfp != nil {
			m.writeStats(statsfp)
		}
		if time.Now().After(tourneyEnd) {
			fmt.Println("\nTourney ended due to lack of time.")
			break
		}
	}
	if matches > 1 {
		fmt.Println()
	}
	if !con.notable {
		ts.print()
	}
}

// vim:ft=go:sts=8:sw=8:noet
