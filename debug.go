// +build debug

// debug logging
// trick from https://dave.cheney.net/2014/09/28/using-build-to-switch-between-debug-and-release
// build with "-tags debug" to activate

package main

import (
	"fmt"
	"os"
)

func dprintf(f string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, f, args...)
}

func dprint(args ...interface{}) {
	fmt.Fprint(os.Stderr, args...)
}

func dprintln(args ...interface{}) {
	fmt.Fprintln(os.Stderr, args...)
}
