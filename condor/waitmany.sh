#!/bin/bash

# condor_wait for multiple job logs

for log; do
    condor_wait -echo $log | grep '^[0-9]' &
done
wait
echo '===DONE==='
