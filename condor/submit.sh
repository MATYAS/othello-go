#!/bin/bash

sub=${1?Need submit file}
gitroot=`git rev-parse --show-toplevel`

cd "$gitroot"
if [[ `uname` == Darwin ]]; then
	make othello_darwin
	cd condor
	mkdir -p result log
	condor_submit $sub os=darwin
else
	make othello_linux
	cd condor
	mkdir -p result log
	condor_submit $sub os=linux
fi
