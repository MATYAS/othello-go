#!/bin/bash

# converts filenames like a___b___c into directory structures like
# a/b/c

# TODO doesn't work w/ files w/ spaces

files=*
if [[ -n $1 ]]; then
	files=$@
fi

for x in $files; do
	[[ -f $x ]] || continue
	[[ $x == *___* ]] || continue
	dir=${x%___*}
	dir=${dir//___/\/}
	name=${x##*___}
	mkdir -p "$dir" && \
		mv "$x" "$dir"/"$name"
done

# vim:ft=sh:sw=8:sts=8:noet
