package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	lo "othello/libothello"
	om "othello/mover"
	"runtime"
	"testing"
	"time"
)

var depthVar uint
var tuminVar, tumaxVar int
var tustepVar int
var runsVar int
var seedVar int64

var emptyargsmap = make(map[string]string, 0)

func init() {
	// flags, mostly for the *Profile benchmarks
	flag.UintVar(&depthVar, "depth", 8, "Minimax depth")
	flag.IntVar(&tuminVar, "tumin", 20, "First turn")
	flag.IntVar(&tumaxVar, "tumax", 55, "Last turn")
	flag.IntVar(&tustepVar, "tustep", 20, "Turns step (test at every N turns) (1 to 55)")
	flag.IntVar(&runsVar, "runs", 15000, "MCTS sim run count")
	flag.Int64Var(&seedVar, "seed", 1, "Random seed (benchmarks only)")

	flag.Parse()

	if tustepVar < 1 || tustepVar > 55 {
		log.Fatalln("-tustep out of range (1 to 55)")
	}
	// TODO range check for tuminVar, tumaxVar
}

func seed() {
	rand.Seed(time.Now().UnixNano())
}

func genRandomBoard() lo.Board {
	white, black := lo.Bitmap(0x0), lo.Bitmap(0x0)
	for i := 0; i < 63; i++ {
		switch rand.Intn(3) {
		case 0:
			white |= lo.Bitmap(0x1)
		case 1:
			black |= lo.Bitmap(0x1)
			// nothing for 2
		}
		white <<= 1
		black <<= 1
	}

	return lo.Board{Current: black, Opponent: white}
}

// genRandomGame returns a game state with at least the given number
// of legal moves.
func genRandomGame(minlegal int) lo.Game {
	for {
		brd := genRandomBoard()
		nempty := lo.Bitcount(brd.EmptyMap())
		g := lo.Game{Board: brd, PieceCount: 64 - nempty}
		if lo.Bitcount(g.Legals()) >= minlegal {
			return g
		}
	}
}

// genInProgressGame returns a game after the given
// number of turns, with both playing randomly.
// (The game may be over if turns is too high.)
func genInProgressGame(turns int) lo.Game {
	var player, opponent om.MonkeyMover
	var move lo.Move

	game := lo.NewGame()
	for i := 0; i < turns; i++ {
		if !game.GameOver {
			move, player, opponent = player.PickMove(&game, time.Duration(10*time.Minute)), opponent, player
			game.ApplyMove(move)
		} else {
			break
		}
	}

	return game
}

/*
This function was written so I could get how many legal moves there were
likely to be (and so what capacity I should make the slices that MLegals()
returns). Redirect it to numlegals.tab, strip out the extra stuff at the end
(like "PASS") and load it into R.

`hist(numlegals$Legals)` gave a nice histogram. I got the 80% mark by doing
something like:

leglist <- numlegals$Legals  # just the Legals column
seqlegals <- seq(1, max(leglist))  # sequence 1, 2, 3, ..., max # of legals
fracbelow <- sapply(seqlegals, function (x) { length(leglist[leglist<=x]) / length(leglist) })
# ^ fraction of observed legals below the given number in the sequence

from looking at that, I saw that around 75% were at 11 or below.

/////

func TestNumLegals(t *testing.T) {
	var player, opponent om.MonkeyMover
	var move Move

	seed()
	fmt.Printf("Turns\tLegals\n")
	for i := 0; i < 1000; i++ {
		for game := lo.NewGame(); !game.gameOver; {
			move, player, opponent = player.PickMove(&game, time.Duration(1*time.Second)), opponent, player
			game.ApplyMove(move)
			_, legalsLen := game.LegalMoves()
			fmt.Printf("%d\t%d\n", game.turnCount, legalsLen)
		}
	}
}

*/
func TestGenRandomBoard(t *testing.T) {
	seed()
	for i := 0; i < 100000; i++ {
		brd := genRandomBoard()
		if !brd.IsValid() {
			t.Fatal("genRandomBoard generated invalid board")
		}
	}
}

func fuzzTestMover(t *testing.T, mvr om.Mover, runs int, legals int) {
	seed()
	for i := 0; i < runs; i++ {
		g := genRandomGame(legals)
		m := mvr.PickMove(&g, time.Duration(10*time.Second))
		if !g.IsLegalMove(m) {
			t.Fatal("player picked illegal move")
		}
		g.ApplyMove(m)
	}
}

func bmMover(b *testing.B, mvr om.Mover, turns int) {
	const nGames = 1000
	var games [nGames]lo.Game
	rand.Seed(seedVar)
	for i := 0; i < nGames; i++ {
		for {
			games[i] = genInProgressGame(turns)
			if !games[i].GameOver {
				break
			}
		}
	}
	runtime.GC()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		g := games[i%nGames] // makes a copy
		m := mvr.PickMove(&g, time.Duration(10*time.Minute))
		g.ApplyMove(m)
	}
}

/*
func BenchmarkWeightedMover(b *testing.B) {
	mvr := &WeightedMover{}
	mvr.Init(make(map[string]string))

	b.ResetTimer()
	seed()
	for _, tu := range []int{10, 30, 45} {
		b.Run(fmt.Sprintf("tu=%d", tu), func(b *testing.B) { bmMover(b, mvr, tu) })
	}
}
*/

func TestMinimaxMover(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in short move")
	}
	mvr := &om.MinimaxMover{}
	mvr.Init(emptyargsmap)
	mvr.Depth = 7
	fuzzTestMover(t, mvr, 200, 0)
}

func TestFuzzIDDFSMover(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in short move")
	}
	mvr := &om.IDDFSMover{}
	mvr.Init(emptyargsmap)
	mvr.Depth = 6
	mvr.TableDepth = 3
	fuzzTestMover(t, mvr, 150, 0)
}

func BenchmarkMinimaxMover(b *testing.B) {
	mvr := &om.MinimaxMover{}
	mvr.Init(emptyargsmap)
	for d := uint(6); d <= 8; d++ {
		mvr.Depth = d
		for _, tu := range []int{10, 30, 43} {
			b.Run(fmt.Sprintf("d=%d tu=%d", d, tu), func(b *testing.B) { bmMover(b, mvr, tu) })
		}
	}
}

func BenchmarkIDDFSMover(b *testing.B) {
	mvr := &om.IDDFSMover{}
	for d := uint(6); d <= 8; d++ {
		mvr.Init(emptyargsmap)
		mvr.Depth = d
		for _, tu := range []int{10, 30, 43} {
			b.Run(fmt.Sprintf("d=%d tu=%d", d, tu), func(b *testing.B) { bmMover(b, mvr, tu) })
		}
	}
}

func BenchmarkProfileMinimax(b *testing.B) {
	mvr := &om.MinimaxMover{}
	mvr.Init(emptyargsmap)
	mvr.Depth = depthVar
	for tu := tuminVar; tu <= tumaxVar; tu += tustepVar {
		b.Run(fmt.Sprintf("d=%d tu=%d", mvr.Depth, tu), func(b *testing.B) { bmMover(b, mvr, tu) })
	}
}

func BenchmarkMinimaxFullGame(b *testing.B) {
	monkey := &om.MonkeyMover{}
	minimax := &om.MinimaxMover{}
	minimax.Init(emptyargsmap)
	con := ConsoleOptions{resultsOnly: true, nopause: true}

	b.ResetTimer()
	rand.Seed(seedVar)
	for d := uint(5); d <= 8; d++ {
		minimax.Depth = d
		b.Run(fmt.Sprintf("d=%d", d), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				white := Player{Name: "white", Mover: monkey}
				black := Player{Name: "black", Mover: minimax}
				var players [2]*Player
				players[lo.Black] = &black
				players[lo.White] = &white
				match := NewMatch(players, time.Duration(10*time.Minute))
				match.run(con)
				if match.forfeited[lo.Black] {
					b.Errorf("d=%d timed out", minimax.Depth)
				}
			}
		})
	}
}

func BenchmarkIDDFSFullGame(b *testing.B) {
	monkey := &om.MonkeyMover{}
	iddfs := &om.IDDFSMover{}
	iddfs.Init(emptyargsmap)
	con := ConsoleOptions{resultsOnly: true, nopause: true}

	b.ResetTimer()
	rand.Seed(seedVar)
	for d := uint(5); d <= 8; d++ {
		iddfs.Depth = d
		b.Run(fmt.Sprintf("d=%d", d), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				white := Player{Name: "white", Mover: monkey}
				black := Player{Name: "black", Mover: iddfs}
				var players [2]*Player
				players[lo.Black] = &black
				players[lo.White] = &white
				match := NewMatch(players, time.Duration(5*time.Minute))
				match.run(con)
				if match.forfeited[lo.Black] {
					b.Errorf("d=%d timed out", iddfs.Depth)
				}
			}
		})
	}
}

func TestMCTSMover(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in short move")
	}
	seed()
	mvr := &om.MCTSMover{}
	mvr.Init(emptyargsmap)
	mvr.Runs = 10000
	mvr.Parallel = true
	t.Run("parallel", func(t *testing.T) { fuzzTestMover(t, mvr, 8, 0) })
	mvr.Parallel = false
	t.Run("parallel", func(t *testing.T) { fuzzTestMover(t, mvr, 8, 0) })
}

func BenchmarkMCTSMover(b *testing.B) {
	mvr := &om.MCTSMover{}
	mvr.Init(emptyargsmap)
	for r := 10000; r <= 40000; r *= 2 {
		mvr.Runs = r
		for _, tu := range []int{10, 30, 43} {
			mvr.Parallel = false
			b.Run(fmt.Sprintf("ser r=%d tu=%d", r, tu), func(b *testing.B) { bmMover(b, mvr, tu) })
			mvr.Parallel = true
			b.Run(fmt.Sprintf("par r=%d tu=%d", r, tu), func(b *testing.B) { bmMover(b, mvr, tu) })
		}
	}
}

func BenchmarkProfileMCTS(b *testing.B) {
	mvr := &om.MCTSMover{}
	mvr.Init(emptyargsmap)
	mvr.Runs = runsVar
	for tu := tuminVar; tu <= tumaxVar; tu += tustepVar {
		b.Run(fmt.Sprintf("par r=%d tu=%d", mvr.Runs, tu), func(b *testing.B) { bmMover(b, mvr, tu) })
	}
}

// vim:ft=go:sw=8:sts=8:noet
